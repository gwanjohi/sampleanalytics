type person = { Name:string;Age:int }

let me = {Age=16;Name="George"}

type personUnion = Age of int | Name of string

let age = Age 56
let name = Name "george"

let anotherMatcher (pUnion) = 
    match pUnion with
    |Age age when age = 34 -> printfn "%s" "agecomponent found"
    |Name name when name = "george" -> printfn "%s" "agecomponent found"



let lastMatcher item =
    match item with
    | Age age when age = 4 -> true
    | Name name -> false
    | _ -> false


let result = lastMatcher age


let matcher person =
    match person with
    | (name) when name ="George" -> printfn "%s" "George Found"
    | _ -> printf "%s" "Could not find george"

#time "on"

let _res = matcher "kimani"
#time "off"

#time
[| 1 .. 10000000 |] |> Array.map (fun x -> x * x)